package fo.nya.meh;

import java.lang.reflect.*;
import java.text.MessageFormat;
import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by 0da on 19.07.2022 4:44; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 */
public class MehGenerator {

    private static final MessageFormat TEMPLATE = new MessageFormat(
            "@FunctionalInterface public interface {0}Meh<{1} extends Throwable>'{'{2} call{3} throws MEH;'}'\n" +
            "@SuppressWarnings('{'\"RedundantThrows\", \"unused\"'}') public static <{1} extends Throwable> {4} of{0}({0}Meh<{1}> f) throws MEH " +
            "'{' return {5} -> '{'try '{'{6}'}' catch (Throwable e) '{'sneaky(e);'}' throw new IllegalStateException(\"This state is impossible.\");'}';'}'"
    );

    public static String generate(Class<?> target) {

        Objects.requireNonNull(target, "Target must be not null.");

        if (!target.isAnnotationPresent(FunctionalInterface.class)) {
            throw new IllegalArgumentException("Target must be @FunctionalInterface [" + target.getCanonicalName() + "]");
        }

        Target method = findMethod(target);

        String simpleName = target.getSimpleName();
        String parameters = Stream.concat(Arrays.stream(target.getTypeParameters()).map(TypeVariable::getName), Stream.of("MEH")).collect(Collectors.joining(", "));

        String result = method.result();
        String arguments = method.arguments();
        String canonical = target.getCanonicalName() + (target.getTypeParameters().length > 0 ? Arrays.stream(target.getTypeParameters()).map(TypeVariable::getName).collect(Collectors.joining(", ", "<", ">")) : "");
        boolean v = method.method.getReturnType() == Void.TYPE;
        String names = IntStream.range(0, method.method.getParameterCount()).mapToObj(MehGenerator::name).collect(Collectors.joining(",", "(", ")"));


        return TEMPLATE.format(new Object[]{simpleName, parameters, result, arguments, canonical, names, v ? ("f.call" + names + ";return;") : ("return f.call" + names + ";")});
    }

    private static String name(int i) {
        StringBuilder a = new StringBuilder();
        do {
            a.insert(0, (char) ('a' + (i % 26)));
            i /= 26;
        } while (i > 0);

        return a.toString();
    }

    private static class Target {
        Class<?> type;
        Map<String, Type> params = new HashMap<>();
        Method method;

        Target(Class<?> type, Type[] params) {
            this.type = type;

            TypeVariable<? extends Class<?>>[] parameters = type.getTypeParameters();
            for (int i = 0; i < parameters.length; i++) {
                this.params.put(parameters[i].getName(), params[i]);
            }
        }

        String arguments() {

            int i = 0;
            StringJoiner joiner = new StringJoiner(", ", "(", ")");
            for (Type parameterType : method.getGenericParameterTypes()) {
                joiner.add(real(parameterType) + " " + name(i++));
            }

            return joiner.toString();
        }

        String result() {
            return real(method.getGenericReturnType()).getTypeName();
        }

        Type real(Type t) {
            return t instanceof TypeVariable ? params.get(((TypeVariable<?>) t).getName()) : t;
        }
    }

    private static Target findMethod(Class<?> target) {
        Queue<Target> targets = new LinkedList<>();
        targets.add(new Target(target, target.getTypeParameters()));

        while (!targets.isEmpty()) {

            Target victim = targets.poll();

            for (Method method : victim.type.getDeclaredMethods()) {
                if (Modifier.isAbstract(method.getModifiers())) {
                    victim.method = method;
                    return victim;
                }
            }

            for (Type type : target.getGenericInterfaces()) {
                ParameterizedType parameterized = (ParameterizedType) type;
                targets.add(new Target((Class<?>) parameterized.getRawType(), parameterized.getActualTypeArguments()));
            }
        }
        throw new IllegalStateException("Class [" + target + "] is missing abstract method");
    }

    public static void main(String[] args) {
        System.out.println(generate(Runnable.class));
        System.out.println(generate(BiConsumer.class));
        System.out.println(generate(BiFunction.class));
        System.out.println(generate(BinaryOperator.class));
        System.out.println(generate(BiPredicate.class));
        System.out.println(generate(BooleanSupplier.class));
        System.out.println(generate(Consumer.class));
        System.out.println(generate(DoubleBinaryOperator.class));
        System.out.println(generate(DoubleConsumer.class));
        System.out.println(generate(DoubleFunction.class));
        System.out.println(generate(DoublePredicate.class));
        System.out.println(generate(DoubleSupplier.class));
        System.out.println(generate(DoubleToIntFunction.class));
        System.out.println(generate(DoubleToLongFunction.class));
        System.out.println(generate(DoubleUnaryOperator.class));
        System.out.println(generate(Function.class));
        System.out.println(generate(IntBinaryOperator.class));
        System.out.println(generate(IntConsumer.class));
        System.out.println(generate(IntFunction.class));
        System.out.println(generate(IntPredicate.class));
        System.out.println(generate(IntSupplier.class));
        System.out.println(generate(IntToDoubleFunction.class));
        System.out.println(generate(IntToLongFunction.class));
        System.out.println(generate(IntUnaryOperator.class));
        System.out.println(generate(LongBinaryOperator.class));
        System.out.println(generate(LongConsumer.class));
        System.out.println(generate(LongFunction.class));
        System.out.println(generate(LongPredicate.class));
        System.out.println(generate(LongSupplier.class));
        System.out.println(generate(LongToDoubleFunction.class));
        System.out.println(generate(LongToIntFunction.class));
        System.out.println(generate(LongUnaryOperator.class));
        System.out.println(generate(ObjDoubleConsumer.class));
        System.out.println(generate(ObjIntConsumer.class));
        System.out.println(generate(ObjLongConsumer.class));
        System.out.println(generate(Predicate.class));
        System.out.println(generate(Supplier.class));
        System.out.println(generate(ToDoubleBiFunction.class));
        System.out.println(generate(ToDoubleFunction.class));
        System.out.println(generate(ToIntBiFunction.class));
        System.out.println(generate(ToIntFunction.class));
        System.out.println(generate(ToLongBiFunction.class));
        System.out.println(generate(ToLongFunction.class));
        System.out.println(generate(UnaryOperator.class));
    }
}
